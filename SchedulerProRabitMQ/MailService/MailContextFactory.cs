﻿using System.Collections.Generic;
using System.Linq;
using CsvReaderService.Message;

namespace MailSender
{
    internal class MailContextFactory
    {
        public static ICollection<MailContext> Create(ReadRecords readRecords)
        {
            return readRecords.Collection.Select(record => new MailContext("SPAM BOT", record.Address, "Special Offer", 
                $@"Hello my {record.Name} {record.Surname}.
Special discount: {record.Discount} is available to {record.DiscountExpire}")).ToArray();
        }
    }
}
