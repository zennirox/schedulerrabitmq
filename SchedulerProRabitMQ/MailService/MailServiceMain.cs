﻿using System;
using Autofac;
using Infrastructure;
using Serilog;
using Serilog.Events;

namespace MailService
{
    internal class MailServiceMain
    {
        static void Main(string[] args)
        {
            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Debug()
                .MinimumLevel.Override("Microsoft", LogEventLevel.Information)
                .Enrich.FromLogContext()
                .WriteTo.Console()
                .CreateLogger();

            var conf = new ConfigureContainer().GenerateContainer();
            using (var container = conf.BeginLifetimeScope())
            {
                var connectionSupport = new ConnectionSupport();
                var connectionResult = connectionSupport.TryConnectToBus(container);
                if (!connectionResult)
                {
                    throw new InvalidOperationException("Failed to establish connection to RabbitMQ server");
                }
                var messageBroker = container.Resolve<IMessageBroker>();
                messageBroker.SetSubscribe();
            }
        }

    }
}
