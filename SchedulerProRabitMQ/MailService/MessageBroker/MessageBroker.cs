﻿using System.Threading.Tasks;
using CsvReaderService.Message;
using Infrastructure;
using MailSender;
using RawRabbit;

namespace MailService.MessageBroker
{
    public class MessageBroker : IMessageBroker
    {
        private readonly IBusClient _busClient;
        private readonly IMailSender _mailSender;

        public MessageBroker(IBusClient busClient, IMailSender mailSender)
        {
            _busClient = busClient;
            _mailSender = mailSender;
        }

        public void SetSubscribe()
        {
            _busClient.SubscribeAsync<ReadRecords>(
                msg => Task.Run(() =>
                {
                    foreach (var mailContext in MailContextFactory.Create(msg))
                    {
                        _mailSender.SentMail(mailContext);
                    }
                }));
        }
    }
}
