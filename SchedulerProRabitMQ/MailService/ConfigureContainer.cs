﻿using System;
using System.IO;
using System.Net;
using System.Net.Mail;
using Autofac;
using Infrastructure;
using MailSender;
using Newtonsoft.Json;
using RawRabbit.DependencyInjection.Autofac;
using Serilog;

namespace MailService
{
    internal class ConfigureContainer : IConfigureContainer
    {
        public IContainer GenerateContainer()
        {
            var builder = new ContainerBuilder();

            var configuration = LoadMailConfiguration("EmailConfiguration.json");

            builder.RegisterInstance<IMailSender>(CreateMailSender(configuration));
            builder.RegisterRawRabbit();
            builder.RegisterType<MessageBroker.MessageBroker>()
                .SingleInstance()
                .As<IMessageBroker>();

            return builder.Build();
        }

        private static MailConfiguration LoadMailConfiguration(string configurationFilePath)
        {
            try
            {
                var jsonFile = File.ReadAllText(configurationFilePath);
                return JsonConvert.DeserializeObject<MailConfiguration>(jsonFile);
            }
            catch (Exception e)
            {
                Log.Error(e,$"Failed to read mail configuration from file: {configurationFilePath}");
                throw;
            }
        }

        private static MailSender.MailSender CreateMailSender(MailConfiguration configuration)
        {
            return new MailSender.MailSender(new SmtpClient(configuration.Smtp, configuration.Port)
            {
                UseDefaultCredentials = false,
                EnableSsl = true,
                Credentials = new NetworkCredential(configuration.Email, configuration.Password)
            });
        }
    }
}
