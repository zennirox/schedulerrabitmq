﻿namespace MailSender
{
    public interface IMailSender
    {
        bool SentMail(MailContext information);
    }
}
