﻿
namespace MailSender
{
    public class MailContext
    {
        public string From { get; }
        public string To { get; }
        public string Subject { get; }
        public string Context { get; }

        public MailContext(string from, string to, string subject, string context)
        {
            From = from;
            To = to;
            Subject = subject;
            Context = context;
        }

        public override string ToString()
        {
            return $"From: {From} \r\n To: {To} \r\n Subject: {Subject} \r\n Context: {Context}";
        }
    }
}
