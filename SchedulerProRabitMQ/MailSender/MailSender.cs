﻿using System;
using System.Net.Mail;
using FluentEmail.Core;
using FluentEmail.Smtp;
using Serilog;

namespace MailSender
{
    public class MailSender : IMailSender
    {
        private readonly SmtpSender _smtpSender;

        public MailSender(SmtpClient smtpClient)
        {
            _smtpSender = new SmtpSender(smtpClient);
        }
        public bool SentMail(MailContext information)
        {
            try
            {
                Log.Information($"Prepare to send email: {information}");
                var email = Email.From(information.From)
                    .To(information.To)
                    .Subject(information.Subject)
                    .Body(information.Context);

                Log.Information("Sending...");
                //_smtpSender.Send(email);
                Log.Information("Mail sent correct.");
                return true;
            }
            catch (Exception e)
            {
                Log.Error(e, "Failed to sent mail");
                return false;
            }
        }
    }
}
