﻿using System.Threading.Tasks;

namespace Infrastructure
{
    public interface IMessageBroker
    {
        void SetSubscribe();
    }

    public interface ISubscribe
    {
        Task SetSubscribe();
    }

    public interface IPublish<in TMessage>
    {
        Task PublicAsync(TMessage message);
    }
}
