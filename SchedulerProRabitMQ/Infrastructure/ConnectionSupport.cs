﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Autofac;
using RawRabbit;
using Serilog;

namespace Infrastructure
{
    public class ConnectionSupport
    {
        public ConnectionSupport()
        {
            MaxRetries = 10;
            Delay = 1000;
        }
        public ConnectionSupport(int maxRetires, int retryDelayMs)
        {
            MaxRetries = maxRetires;
            Delay = retryDelayMs;
        }
        public int MaxRetries { get; }
        public int Delay { get; }

        public Task<bool> TryConnectToBusAsync(ILifetimeScope container)
        {
            return Task.Run(() => TryConnectToBus(container));
        }

        public bool TryConnectToBus(ILifetimeScope container)
        {
            var resolved = false;
            var retries = 0;
            do
            {
                try
                {
                    container.Resolve<IBusClient>();
                    resolved = true;
                }
                catch (Exception ex)
                {
                    Thread.Sleep(Delay);
                    retries++;
                    Log.Error(ex, $"Failed to initialize {nameof(IBusClient)} broker");
                }

                if (retries == MaxRetries)
                {
                    return false;
                }
            } while (!resolved);

            return true;
        }
    }
}
