﻿using Autofac;

namespace Infrastructure
{
    public interface IConfigureContainer
    {
        IContainer GenerateContainer();
    }
}
