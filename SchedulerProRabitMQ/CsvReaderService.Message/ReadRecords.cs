﻿using System.Collections.Generic;
using System.Collections.Immutable;

namespace CsvReaderService.Message
{
    public class ReadRecords
    {
        public IImmutableList<ReadRecord> Collection { get; }

        public ReadRecords(IEnumerable<ReadRecord> collection)
        {
            Collection = collection.ToImmutableList();
        }
        public static ReadRecords Empty => new ReadRecords(new List<ReadRecord>());
    }
}
