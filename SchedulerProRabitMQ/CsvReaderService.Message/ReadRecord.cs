﻿using System;

namespace CsvReaderService.Message
{
    public class ReadRecord
    {
        public string Name { get; }

        public string Surname { get; }

        public string Address { get; }

        public double Discount { get; }

        public DateTime DiscountExpire { get; }

        public ReadRecord(
            string name, 
            string surname,
            string address, 
            double discount,
            DateTime discountExpire)
        {
            Name = name;
            Surname = surname;
            Address = address;
            Discount = discount;
            DiscountExpire = discountExpire;
        }
    }
}
