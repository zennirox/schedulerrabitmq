﻿using System.Threading.Tasks;
using Quartz;
using RawRabbit;
using SchedulerService.Message;

namespace SchedulerService.QuartzIntegration
{
    public class Job : IJob
    {
        private readonly IBusClient _busClient;

        public Job(IBusClient busClient)
        {
            _busClient = busClient;
        }
        public Task Execute(IJobExecutionContext context)
        {
            return _busClient.PublishAsync(new TimeElapsed());
        }
    }
}
