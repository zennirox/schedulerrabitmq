﻿namespace SchedulerService.QuartzIntegration
{
    public interface ISchedulerWorker
    {
        void Start(int jobIntervalSeconds);
        void Stop();
    }
}
