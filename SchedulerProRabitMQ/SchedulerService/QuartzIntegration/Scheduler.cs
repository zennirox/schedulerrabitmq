﻿using Quartz;
using Quartz.Impl;
using Quartz.Spi;
using Serilog;

namespace SchedulerService.QuartzIntegration
{
    public class Scheduler : ISchedulerWorker
    {
        private readonly IScheduler _scheduler;

        public Scheduler(IJobFactory factory)
        {
            _scheduler = new StdSchedulerFactory().GetScheduler().Result;
            _scheduler.JobFactory = factory;
        }

        public async void Start(int jobInterval)
        {
            try
            {
                //Logger np do Nloga
                //LogProvider.SetCurrentLogProvider(new ConsoleLogProvider());
                var job = JobBuilder.Create<IJob>().Build();
                var trigger = TriggerBuilder.Create()
                    .WithSimpleSchedule(x => x
                        .WithIntervalInSeconds(jobInterval)
                        .RepeatForever())
                    .Build();

                await _scheduler.ScheduleJob(job, trigger);
                await _scheduler.Start();
            }
            catch (SchedulerException ex)
            {
                Log.Error(ex, "Failed to start scheduler");
            }
        }
        public void Stop()
        {
            _scheduler.Shutdown();
        }
    }
}
