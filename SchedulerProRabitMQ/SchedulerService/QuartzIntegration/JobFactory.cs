﻿using Quartz;
using Quartz.Spi;

namespace SchedulerService.QuartzIntegration
{
    public class JobFactory : IJobFactory
    {
        private readonly IJob _job;

        public JobFactory(IJob job)
        {
            _job = job;
        }
        public IJob NewJob(TriggerFiredBundle bundle, IScheduler scheduler)
        {
            return _job;
        }

        public void ReturnJob(IJob job)
        {
            //throw new System.NotImplementedException();
        }
    }
}
