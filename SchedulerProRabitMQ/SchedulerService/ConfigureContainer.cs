﻿using Infrastructure;
using Autofac;
using Quartz;
using Quartz.Spi;
using RawRabbit.DependencyInjection.Autofac;
using SchedulerService.QuartzIntegration;

namespace SchedulerService
{
    internal class ConfigureContainer : IConfigureContainer
    {
        public IContainer GenerateContainer()
        {
            var builder = new ContainerBuilder();
            builder.RegisterRawRabbit();
            builder.RegisterType<Job>().As<IJob>();
            builder.RegisterType<JobFactory>().As<IJobFactory>();
            builder.RegisterType<Scheduler>().As<ISchedulerWorker>();


            return builder.Build();
        }
    }
}
