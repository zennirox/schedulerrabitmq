﻿using System;
using System.Threading;
using Autofac;
using Infrastructure;
using SchedulerService.QuartzIntegration;
using Serilog;
using Serilog.Events;

namespace SchedulerService
{
    internal class SchedulerServiceMain
    {
        static void Main(string[] args)
        {
            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Debug()
                .MinimumLevel.Override("Microsoft", LogEventLevel.Information)
                .Enrich.FromLogContext()
                .WriteTo.Console()
                .CreateLogger();

            var conf = new ConfigureContainer();
            using (var container = conf.GenerateContainer())
            {
                var connectionSupport = new ConnectionSupport();
                var connectionResult = connectionSupport.TryConnectToBus(container);
                if (!connectionResult)
                {
                    throw new InvalidOperationException("Failed to establish connection to RabbitMQ server");
                }
                var scheduler = container.Resolve<ISchedulerWorker>();
                scheduler.Start(5);
                Thread.Sleep(-1);
            }
        }
    }
}
