﻿using System;

namespace CsvReaderService.Reading
{
    public class Record
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Address { get; set; }
        public float Discount { get; set; }
        public DateTime DiscountExpire { get; set; }
    }
}
