﻿using System;

namespace CsvReaderService.Reading
{
    public static class RecordExt
    {
        public static bool IsDiscountValid(this Record record)
        {
            return record.DiscountExpire > DateTime.Now;
        }
    }
}
