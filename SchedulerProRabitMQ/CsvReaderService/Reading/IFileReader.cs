﻿using System.Threading.Tasks;

namespace CsvReaderService.Reading
{
    public interface IFileReader<T> 
    {
        T[] ReadRecords(int maxRecordsToRead);
        Task<T[]> ReadRecordsAsync(int maxRecordsToRead);
        T[] ReadRecords();
        bool TryReadRecords(int maxRecordsToRead, out T[] records);

    }
}
