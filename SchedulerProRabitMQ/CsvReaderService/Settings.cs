﻿namespace CsvReaderService
{
    public class Settings
    {
        public string PathToFile { get; }
        public int RecordsToReadAtOnce { get; }

        public Settings(string pathToFile, int recordsToReadAtOnce)
        {
            PathToFile = pathToFile;
            RecordsToReadAtOnce = recordsToReadAtOnce;
        }
    }
}
