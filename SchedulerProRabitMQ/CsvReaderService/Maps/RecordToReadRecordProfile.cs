﻿using System.Collections.Generic;
using AutoMapper;
using CsvReaderService.Message;
using CsvReaderService.Reading;

namespace CsvReaderService.Maps
{
    public class RecordToReadRecordProfile : Profile
    {
        public RecordToReadRecordProfile()
        {
            CreateMap<Record,ReadRecord>();
        }
    }
}
