﻿using CsvHelper.Configuration;
using CsvReaderService.Reading;

namespace CsvReaderService.Maps
{
    public sealed class RecordMap: ClassMap<Record>
    {
        public RecordMap()
        {
            Map(m => m.Name);
            Map(m => m.Surname);
            Map(m => m.Address);
            Map(m => m.Discount);
            Map(m => m.DiscountExpire).TypeConverterOption.Format("dd.MM.yyyy");
        }
    }
}