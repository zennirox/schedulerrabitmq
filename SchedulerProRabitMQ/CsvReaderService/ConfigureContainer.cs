﻿using System;
using System.Collections.Generic;
using Autofac;
using AutoMapper;
using CsvReaderService.Reading;
using Infrastructure;
using RawRabbit.DependencyInjection.Autofac;

namespace CsvReaderService
{
    internal class ConfigureContainer : IConfigureContainer
    {
        public IContainer GenerateContainer()
        {
            var builder = new ContainerBuilder();
            builder.RegisterRawRabbit();
            var readerWrapper = new CsvReaderWrapper<Record>(@"database.csv");
            builder.RegisterInstance(readerWrapper).As<IFileReader<Record>>();
            builder.RegisterType<MessageBroker>().As<IMessageBroker>();

            var assemblies = AppDomain.CurrentDomain.GetAssemblies();
            builder.RegisterAssemblyTypes(assemblies)
                .Where(t => typeof(Profile).IsAssignableFrom(t) && !t.IsAbstract && t.IsPublic)
                .As<Profile>();

            builder.Register(c => new MapperConfiguration(cfg => {
                foreach (var profile in c.Resolve<IEnumerable<Profile>>())
                {
                    cfg.AddProfile(profile);
                }
            })).AsSelf().SingleInstance();

            builder.Register(c => c.Resolve<MapperConfiguration>()
                    .CreateMapper(c.Resolve))
                .As<IMapper>()
                .InstancePerLifetimeScope();

            var settings = new Settings("",100); //TODO: call to json to get settings
            builder.RegisterInstance(settings);
            return builder.Build();
        }
    }
}
