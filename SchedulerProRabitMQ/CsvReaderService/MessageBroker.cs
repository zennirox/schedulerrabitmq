﻿using System.Threading.Tasks;
using AutoMapper;
using CsvReaderService.Message;
using CsvReaderService.Reading;
using Infrastructure;
using RawRabbit;
using SchedulerService.Message;

namespace CsvReaderService
{
    public class MessageBroker : IMessageBroker
    {
        private readonly IBusClient _busClient;
        private readonly IFileReader<Record> _fileReader;
        private readonly IMapper _mapper;
        private readonly Settings _configuration;

        public MessageBroker(
            IBusClient busClient, 
            IFileReader<Record> fileReader,
            IMapper mapper,
            Settings configuration
            )
        {
            _busClient = busClient;
            _fileReader = fileReader;
            _mapper = mapper;
            _configuration = configuration;
        }

        public void SetSubscribe()
        {
            _busClient.SubscribeAsync<TimeElapsed>(msg => Publish(GetRecords()));
        }

        public Task Publish(ReadRecords readRecords)
        {
            return _busClient.PublishAsync(readRecords);
        }
        public ReadRecords GetRecords()
        {
            var isCorrectRead = _fileReader.TryReadRecords(_configuration.RecordsToReadAtOnce, out var readRecords); //TODO: map to msg
            return isCorrectRead
                ? new ReadRecords(_mapper.Map<ReadRecord[]>(readRecords)) 
                : ReadRecords.Empty;
        }
    }
}
